# Homeautomation powered by openHAB.
I'll provide here some basic snippets, etc. for your personal homeautomation powered by [openHAB](https://www.openhab.org/).

Feel free to use and enjoy.

#  Preface
I ran my first openHAB instance on a [Hardkernel ODROID-C1](https://www.hardkernel.com/main/products/prdt_info.php?g_code=G141578608433) with  1-wire connected to GPIO and an DIY [EIB/KNX IP Gateway and Router](https://michlstechblog.info/blog/raspberry-pi-eibknx-ip-gateway-or-router-with-the-pi/) connected to GPIO as well.
But with upcoming request to the UI, graphs, time series databases and IoT-devices like Netatmo and Alexa, I decided to set up a completely fresh infrastructure for my homeautomation running as containers on Docker; my new DIY NAS is capable of running Docker containers, why not accomplish this task with Docker, right?

# Let's Dock the infrastructure for openHAB and friends.
Let's define a setup for openHAB but what are the requirements the homeautomation system has to fullfill?

## Requirements
Think about these requirements, needs and nice to have services, we want our homeautomation system to deal with:

- EIB/KNX installation bus
- 1-wire sensors
- store data in time series database
- show data in graphs
- interact with IoT-devices
- integrate a CCTV system
- interact with Cloud services like IFTTT
- We do not re-invent the wheel !
- Run on Docker !

## Let's go !
Let's accomplish that mission. First of all I'll look if there are any Docker image matching my needs, because ...
> We do not re-invent the wheel !

I'll have at the [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices) and believe it's best to [decouple applications](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#decouple-applications)
> Each container should have only one concern.
> The container should be stateless.

For production environments I recommend to have a look at the image size and the runtime footprint because the size does matter; I'm sorry about that.

Have a look at the folder "openhab_on_docker" inside this repository for further details on how to run the Docker images, hwo to persist data, etc.

These are the generic steps to get an container up and running and to persists the containers data.
1. Create a directory for the application.
    This directory may hold the service-files, docker-compose files, etc. as well.
2. Create a directory for the application's data like configurations, etc.
    We will mount this directory to a directory inside of the container to persist the container data. So the data will stay even if the container gets removed, updated, etc.
    > The container should be stateless.
3. Run the Docker container for the application.
4. Register the application as a service.

### Dock openHAB
Let's start with the essential container. openHAB provides a lot of pre-built Docker images on [Docker Hub: openhab/openhab](https://hub.docker.com/r/openhab/openhab/). 
openHAB 1.x and 2.x are supported on Docker. We just have to decide if we want to use a small alpine-based image or a bigger debian-based image. So, we will run the alpine based image with the smaller footprint.
